# fluxbox-git

Fluxbox is a window manager for X

## Archlinux package

This project automaticaly build an archlinux package for the latest **fluxbox** git version. Source is the github [fluxbox](https://github.com/fluxbox/fluxbox) mirror repo.

## Download

You can download **fluxbox-git** archlinux package from here:

* [fluxbox-git-*-any.pkg.tar.zst](https://gitlab.com/archlinux_build/fluxbox-git/-/jobs/artifacts/main/browse?job=run-build)

## Install

eg. 20230507

```bash
sudo pacman -U fluxbox-git-20230507-1-any.pkg.tar.zst

```


## License
For open source projects, say how it is licensed.
